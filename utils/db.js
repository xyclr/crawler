/**
 * @Author goujianjun
 * @Since v1.0.0
 */
/**
 * 链接数据库
 */
const {dbUrl} = require('../config')
const logger = require('../utils/logger')
var mongoose = require('mongoose');

/**
 * 连接
 */
mongoose.connect(dbUrl);

/**
 * 连接成功
 */
mongoose.connection.on('connected', function () {
  logger.info('Mongoose connection open to ' + dbUrl);
});

/**
 * 连接异常
 */
mongoose.connection.on('error',function (err) {
  logger.error('Mongoose connection error: ' + err);
});

/**
 * 连接断开
 */
mongoose.connection.on('disconnected', function () {
  logger.info('Mongoose connection disconnected');
});

module.exports = mongoose;