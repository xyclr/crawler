const Total = require("../model/total.js")
const logger = require('../utils/logger')

const TOTAL_TYPE = {
  0: 'interest',
  1: 'restaurant',
  2: 'img',
  3: 'url',
  4: 'translate',
}


/**
 *
 * interesting : 0, restaurent: 1, img: 2, url: 3
 */
const totalInfoHandle ={
  addError: err => {
    // let type= TOTAL_TYPE[err.type]
    // global.total[`${type}Error`].push(err)

    global.total.error[err.type].push(err)
    totalInfoHandle.show()
    Total.update({},{'$push':{'error':err}},{upsert: true},function(err, docs){
      if (err) return console.log('add total error err ',err);
    })
  },
  add: item => {
    // let type= TOTAL_TYPE[item.type]
    // global.total[`${type}Total`].push(item)
    global.total.all[item.type].push(item)
    totalInfoHandle.show()
  },

  addSuccess: item => {
    // let type= TOTAL_TYPE[item.type]
    // global.total[`${type}Success`].push(item)

    global.total.success[item.type].push(item)
    totalInfoHandle.show()
  },
  show:() => {
    let {all, success, error} = global.total
    let result = ''
    Object.keys(global.total).forEach((v, k) => {
      Object.keys(global.total[v]).forEach((item, key) => {
        result +=  `${TOTAL_TYPE[key]} ${v}: ${global.total[v][item].length} ,`
      })
    })

    logger.info(result)
    logger.info('---------------')
  },
  clear: () => {
    Total.remove({}, function (err) {
      if (err) {
        console.error('delete total error', err);
      } else {
        console.error("delete total success")
      }
    });
  }
}
module.exports = totalInfoHandle