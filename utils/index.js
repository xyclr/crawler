/**
 * @Author goujianjun
 * @Since v1.0.0
 */
const fs = require('fs')

const {imgUrl, interestingUrl, imgProcessType} = require('../config')

//同步创建目录
const deleteFolder = path => {
  var files = [];
  if( fs.existsSync(path) ) {
    files = fs.readdirSync(path);
    files.forEach(function(file,index){
      var curPath = path + "/" + file;
      if(fs.statSync(curPath).isDirectory()) { // recurse
        deleteFolder(curPath);
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
}

const translateUrlToAbsolute = (url, host) => {
  let result = url
  if (url.indexOf('http://') === -1 && url.indexOf('https://') === -1) {
    if (url.indexOf('//') !== -1) {
      result = url.replace('//', 'http://')
    } else {
      result = host + url
    }
  }
  return result
}

const processImg = (url, type) => {
  let newSrc = (imgUrl + url.split('/').pop()) + '?' + type
  return newSrc
}
module.exports = {
  deleteFolder,
  translateUrlToAbsolute,
  processImg
}