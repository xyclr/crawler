const fs = require('fs')
const logger = require('./utils/logger')
const {cPromise} = require('./utils/myPromise')
const {host} = require('./config')
const crawlRestaurant = require('./tasks/crawlRestaurant')
const crawlInteresting = require('./tasks/crawlInteresting')
const {interestingUrl, hotUrl} = require('./config')
const {deleteFolder} = require('./utils')
const totalInfoHandle = require('./utils/totalInfoHandle')

const HOT_PAGES = 5


const TOTAL_TYPE = {
  0: 'interest',
  1: 'restaurant',
  2: 'img',
  3: 'url',
  4: 'translate'
}

global.total = {
  all: {
    0: [],
    1: [],
    2: [],
    3: [],
    4: [],
  },
  success: {
    0: [],
    1: [],
    2: [],
    3: [],
    4: [],
  },
  error: {
    0: [],
    1: [],
    2: [],
    3: [],
    4: [],
  }
}

// fs.writeFileSync('./logs/cheese.log', '');
// totalInfoHandle.clear()

const sleep = (time) => {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      // 返回 ‘ok’
      resolve('ok');
    }, time);
  })
};

/**
 *
 * @param baseUrl
 * @param count 页数
 * @param queries
 */
const crawlHotPages = ({baseUrl, count, queries}) => {
  let pageNum = 1
  let regionsQuery = queries.regions.split(',').map(item => `regions=${item}`).join('&')
  let resLen = 0
  const getPage = async () => {
    let regions = queries.regions
    let url = `${baseUrl}?${regionsQuery}&domain=1&page.number=${pageNum}`
    cPromise(url).then(async $ => {
      let $previousePageLink = $('#previousePageLink')
      let $newxtPageLink = $('#nextPageLink')
      logger.info('crawlHotPages', url, pageNum)
      if ($newxtPageLink.length > 0 || $previousePageLink.length > 0) {
        let urls = []
        $('._1jEXXmnQl5Qh85EEe9qgoR').each((k, v) => {
          let restaurantUrl = host + $(v).attr('href')
          urls.push(restaurantUrl)
        })
        for (let v of urls) {
          try {
            console.log('resLen:', resLen++)
            await crawlRestaurant(v, {
              regions
            })
            logger.info(`crawl restaurant ${url} success`)
            totalInfoHandle.addSuccess({
              type: 1,
              url: v,
              regions
            })
          } catch (error) {
            logger.error(error, `crawl restaurant ${url} error`)
            totalInfoHandle.addError({
              type: 1,
              url: v,
              regions,
              error
            })
          }
        }

      }
      if ($newxtPageLink.length > 0 && pageNum < count) {
        setTimeout(() => {
            pageNum++
            getPage()
          },
          10000)

      }
    })
  }

  getPage()
}


const crawlHot = () => {
  hotUrl
    .forEach(item => {
      crawlHotPages({
        baseUrl: 'https://www.wongnai.com/businesses',
        count: HOT_PAGES,
        queries: {
          regions: item.regions
        }
      })
    })
}


// 有趣的
const crawlInterestingList = (urls) => {
  let interestingLen = 0
  Promise
    .all(urls.map(item => cPromise(item.url)))
    .then(data => {
      data.forEach(($, k) => {
        let regionId = interestingUrl[k].regionId
        let name = interestingUrl[k].name
        let url = interestingUrl[k].url
        let $target = $('._3aAlLTOKklml6chlNkYjTx .ueDHYfZN4LhsL_flQsDVu')
        let len = $target.length
        logger.info(`crawl ${name} ${url}, 总条数 ${len}`)
        let urls = []
        $target.each((k, v) => {
          let url = host + $(v).attr('href')
          urls.push(url)
        });

        (async () => {
          for(let v of urls) {

            try {
              console.log('interestingLen:', interestingLen++)
              await crawlInteresting(v, {regionId})
              totalInfoHandle.addSuccess({
                type: 0,
                url:v,
                regionId
              })
            } catch (err) {
              totalInfoHandle.addError({
                type: 0,
                url:v,
                regionId,
                error:err
              })
            }
          }
        })()


      })
    }, err => {
      logger.error('crawl interesting err', err)
    })
}


crawlHot()
// crawlInterestingList(interestingUrl)

/**/
// crawlInteresting('http://www.wongnai.com/articles/yoktalay-seafood-chonburi', {regionId: 111}).then(data => {
//   console.log('success')
// 	totalInfoHandle.addSuccess({
// 		type: 0,
// 		url:'http://www.wongnai.com/articles/paul-siam-paragon-by-scb'
// 	})
// }, err=> {
//   console.log(err)
// 	totalInfoHandle.addError({
// 		type: 0,
// 		url:'http://www.wongnai.com/articles/paul-siam-paragon-by-scb'
// 	})
// })
// crawlRestaurant('https://www.wongnai.com/restaurants/287921eB-heekcaa-siam-discovery-%E0%B8%8A%E0%B8%B1%E0%B9%89%E0%B8%99-2',
//   {
//   regions: [9081]
// }).then(data => {
//   console.log('success')
//   totalInfoHandle.addSuccess({
//     type: 1,
//     url:'http://www.wongnai.com/articles/paul-siam-paragon-by-scb'
//   })
// }, err=> {
//   console.log(err)
//   totalInfoHandle.addError({
//     type: 1,
//     url:'http://www.wongnai.com/articles/paul-siam-paragon-by-scb'
//   })
// })

// cPromise('http://www.wongnai.com/_api/restaurants/287921eB-heekcaa-siam-discovery-%E0%B8%8A%E0%B8%B1%E0%B9%89%E0%B8%99-2/photos.json?_v=5.044&locale=th&type=14&page.size=24')
//   .then(res => {
//     let data = res.toJSON()
//     debugger
//   })


