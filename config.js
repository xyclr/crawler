exports = module.exports = {
  host: 'http://www.wongnai.com/',
  crawlerConfig: {
    rateLimit: 2000,
    maxConnections: 5,
    encoding: null,
    retries:10,
    retryTimeout: 1000,
  },
  // dbUrl: 'mongodb://localhost:27017/wongnai',
  dbUrl: 'mongodb://wongnai:wongnai@123.57.80.184:27017/wongnai',
  imgUrl: 'http://ox6rr1wlm.bkt.clouddn.com/',
  imgProcessType: {
    // 有趣的 裁剪 + 水印
    s1: 'imageMogr2/auto-orient/thumbnail/1080x/gravity/North/crop/x1200/blur/1x0/quality/75|watermark/1/image/aHR0cDovL294NnJyMXdsbS5ia3QuY2xvdWRkbi5jb20vV2VjaGF0SU1HMjQxLnBuZw==/dissolve/100/gravity/SouthEast/dx/10/dy/10|imageslim',
    // 水印
    s2: 'imageMogr2/auto-orient/thumbnail/1080x/blur/1x0/quality/75|watermark/1/image/aHR0cDovL294NnJyMXdsbS5ia3QuY2xvdWRkbi5jb20vV2VjaGF0SU1HMjQxLnBuZw==/dissolve/100/gravity/SouthEast/dx/10/dy/10|imageslim',
    s3: 'imageView2/0/format/webp/q/25|imageslim',
  },
  interestingUrl: [
    {
      url: 'https://www.wongnai.com/articles?domain=1&regionId=9681',
      regionId: '9681',
      name: '曼谷'
    },
    {
      url: 'https://www.wongnai.com/articles?domain=1&regionId=843',
      regionId: '843',
      name: '普吉岛'
    },
    {
      url: 'https://www.wongnai.com/articles?domain=1&regionId=269',
      regionId: '269',
      name: '芭提雅'
    }
  ],
  hotUrl: [
    {
      name: '曼谷',
      regions: '9681'
    },
    {
      name: '普吉',
      regions: '373'
    },
    {
      name: '清迈',
      regions: '843'
    },
    {
      name: '芭提雅',
      regions: '1009,1025,1027,41924,41940,49107'
    }],
}