$(function () {
  $('#select').change(function () {
    let val = $(this).children('option:selected').val()
    window.location.href = "/interesting?regions=" + val
  });
})