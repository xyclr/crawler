$(function () {
  $('.J-gather').click(function(){
    var $me = $(this);
    var type = $me.attr('item-type');
    var id = $me.attr('item-id');
    var gathered = $me.attr('item-gathered') === '1';
    var newGathered = {
      value: !gathered ? '1' : '0',
        text: !gathered ? '已采集' : '未采集'
    };

    $.ajax({
        url: `/gather?type=${type}&id=${id}&gathered=${newGathered.value}`,
        method: 'get',
        success: function(data) {
          console.log(data)

            $me.text(newGathered.text)
            $me.attr({'item-gathered' : newGathered.value})
        }
    })

  })

    $('.J-style-toggle').click(function(){
        var $me = $(this);
        var visiable = $('.s1').css('display') === 'block'
        if(visiable) {
            $('.s1').css('display', 'none')
            $('.s2').css('display', 'block')
            $me.text('无样式')
        } else {
            $('.s2').css('display', 'none')
            $('.s1').css('display', 'block')
            $me.text('有样式')
        }

    })


})