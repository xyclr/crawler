var express = require('express');
var router = express.Router();
let restaurant = require('../model/restaurant')
const {imgUrl, hotUrl, imgProcessType} = require('../config')
const {processImg} = require('../utils')

router.get('/', function (req, res, next) {
  let {id} = req.query
  restaurant.findOne({_id: id}, {}, function (err, docs) {
    if (err) {

    } else {
      let newContent = docs.brandStory[0].content.map(item => {
        if(item.tag === 'p') {
          return item
        }

        if(item.tag === 'figure') {
          return Object.assign(
            item,
            {
              src: processImg(item.src, imgProcessType.s1),
            }
          )
        }

        if(item.tag === 'div') {
          return Object.assign(
            item,
            {
              imgs: item.imgs.map(v => {
                return {
                  text: v.text,
                  src: processImg(v.src, imgProcessType.s1),
                }
              })
            }
            
          )
        }
      })
      res.render('brandStory', {
        data: Object.assign(docs.brandStory[0], {
          mainPic: {
            text: docs.brandStory[0].mainPic.text,
            src: processImg(docs.brandStory[0].mainPic.src, imgProcessType.s1),
          },
          content: newContent
        })
      });
    }
  })
});

module.exports = router;
