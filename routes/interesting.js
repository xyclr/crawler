var express = require('express');
var router = express.Router();
let interesting = require('../model/interesting')
const {imgUrl,hotUrl, interestingUrl,imgProcessType} = require('../config')
const {processImg} = require('../utils')

router.get('/', function(req, res, next) {
  let {regions = interestingUrl[0].regionId} = req.query

  interesting.find({regions: [regions]}, {
    _id:1,
    title: 1,
    content: 1,
      gathered:  1,
  }, function(err, docs){
    if(err) {

    } else {
      res.render('interesting', {
        select: interestingUrl,
        regions,
        list: docs.map(item => {
          let mainPic = ''
          try  {
            mainPic = processImg(item.content.filter(v => {
              return v.tag === 'figure'
            })[0].src, imgProcessType.s3)
          } catch (e) {
            console.log(e)
          }
          return Object.assign(item, {
             interestUrl: 'interest?id=' + item._id,
            mainPic
          })
        })
      });
    }
  })
});

router.get('/gathered', function(req, res, next) {
    let {gathered, id} = req.query
    interesting.update({_id: id}, {$set:{
        gathered:gathered === 0 ? false : true
    }}, function(err, docs){
        if(err) {
            JSON.stringify({
                code: 0,
                msg: 'err'
            })
        } else {
            res.end(
                JSON.stringify({
                    code: 1,
                    msg: 'success'
                })
            );
        }
    })
});
module.exports = router;
