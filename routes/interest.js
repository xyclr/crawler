var express = require('express');
var router = express.Router();
let interesting = require('../model/interesting')
let restaurant = require('../model/restaurant')
const {imgUrl, interestingUrl,imgProcessType} = require('../config')
const {processImg} = require('../utils')

router.get('/', function(req, res, next) {
    let {id} = req.query
  interesting.findOne({_id: id}, {}, function (err, docs) {
    if(err) {

    } else {
      docs.content.map(item => {
        if(item.tag === 'p') {
          return item
        }

        if(item.tag === 'figure') {
          return Object.assign(
            item,
            {
              src: processImg(item.src, imgProcessType.s1),
              srcOrigin: item.src,
            }
          )
        }

        if(item.tag === 'div') {
          return Object.assign(
            item,
            {
              imgs: item.imgs.map(v => {
                return {
                  text: v.text,
                  src: processImg(v.src, imgProcessType.s1),
                  srcOrigin: v.src,
                }
              })
            }

          )
        }
      })

      let newDoc = Object.assign(docs, {})
      newDoc._doc._id = newDoc._doc._id + ''


      restaurant.findOne({url: 'https://www.wongnai.com/' + newDoc.restaurantInfo.url}, {}, function (err, restaurantDoc) {
        console.log(restaurantDoc)
        if (err) {

        } else {
          console.log(restaurant)
          res.render('interest', {
            data: newDoc,
            restaurant: restaurantDoc
          });
        }
      })
    }
  })
});



module.exports = router;
