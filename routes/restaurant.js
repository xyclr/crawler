var express = require('express');
var router = express.Router();
let restaurant = require('../model/restaurant')
const {imgUrl, hotUrl, imgProcessType} = require('../config')
const {processImg} = require('../utils')

router.get('/', function (req, res, next) {
  let {id} = req.query
  restaurant.findOne({_id: id}, {}, function (err, restaurant) {
    if (err) {

    } else {
      restaurant.mainPic = processImg(restaurant.mainPic, imgProcessType.s2),

      restaurant.menu = restaurant.menu.map(item => {
        return {
          src: processImg(item.src, imgProcessType.s2),
          srcOrigin: item.src,
          title: item.title
        }
      })

      restaurant.menuRecommend = restaurant.menuRecommend.map(item => {
        return {
          imgs: item.imgs.map(img => {
            return {
              title: img.title,
              src: processImg(img.src, imgProcessType.s2),
              srcOrigin: item.src,
            }
          }),
          name: item.name,
          nameCn: item.nameCn,
        }
      })

      restaurant.brandStory = restaurant.brandStory.map(v => {
        let newContent = v.content.map(item => {
          if(item.tag === 'p') {
            return item
          }

          if(item.tag === 'figure') {
            return Object.assign(
              item,
              {
                src: processImg(item.src, imgProcessType.s1),
              }
            )
          }

          if(item.tag === 'div') {
            return Object.assign(
              item,
              {
                imgs: item.imgs.map(v => {
                  return {
                    text: v.text,
                    src: processImg(v.src, imgProcessType.s1),
                  }
                })
              }

            )
          }
        })


        return Object.assign(v, {
          mainPic: {
            text: v.mainPic.text,
            src: processImg(v.mainPic.src, imgProcessType.s1),
          },
          content: newContent
        })
      })


      restaurant['brandStoryUrl'] = '/brandStory?id=' + id
      restaurant['imgs13'] = []
      restaurant['imgs14'] = []
      restaurant.imgs = restaurant.imgs.map(item => {
        let type = item.type
        let re = {
          src:processImg(item.src, imgProcessType.s2),
          srcOrigin: item.src,
          text: item.text
        }
        restaurant[`imgs${type}`].push(re)
        return re
      })

      restaurant.comments = restaurant.comments.map(item => {
        let imgs = item.imgs.map(i => {
          return {
            src: processImg(i.src, imgProcessType.s2),
            srcOrigin: i.src,
            text: i.text
          }
        })

        return Object.assign(item, {imgs})
      })



      restaurant['daren'] = restaurant.comments.find(item => item.menu.length > 0)

      let newDoc = Object.assign(restaurant, {})
      newDoc._doc._id = newDoc._doc._id + ''
      res.render('restaurant', {
        restaurant :newDoc
      });
    }
  })
});

module.exports = router;
