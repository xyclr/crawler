var express = require('express');
var router = express.Router();
let restaurant = require('../model/restaurant')
const {imgUrl, hotUrl,imgProcessType} = require('../config')
const {processImg} = require('../utils')

router.get('/', function(req, res, next) {
  let {regions = hotUrl[0].regions} = req.query
  restaurant.find({regions: regions.split(',')}, {
    _id:1,
    url: 1,
    name: 1, // 点名
    nameCn: 1, // 点名
    gathered:  1,
    classification: 1, //分类
    mainPic: 1, // 主图
  }, function(err, docs){
    if(err) {

    } else {
      res.render('hot', {
        select: hotUrl,
        regions,
        list: docs.map(item => {
          return Object.assign(item, {
            restaurantUrl: 'restaurant?id=' + item._id,
            mainPic:processImg(item.mainPic, imgProcessType.s3),
          })
        })
      });
    }
  })
});

router.get('/gathered', function(req, res, next) {
  let {gathered, id} = req.query
  restaurant.update({_id: id}, {$set:{
      gathered:gathered === 0 ? false : true
  }}, function(err, docs){
    if(err) {
        JSON.stringify({
            code: 0,
            msg: 'err'
        })
    } else {
      res.end(
        JSON.stringify({
            code: 1,
            msg: 'success'
        })
      );
    }
  })
});

module.exports = router;
