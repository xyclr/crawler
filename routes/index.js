var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.cookie('name', 'x', {maxAge:600000, httpOnly:true, path:'/', secure:false});
  res.render('index', '');
});



module.exports = router;
