var express = require('express');
var router = express.Router();
let restaurant = require('../model/restaurant')
let interesting = require('../model/interesting')

/**
 * interesting:0 restaurant:1
 */
router.get('/', function(req, res, next) {
    let {gathered, id, type} = req.query
    let newGathered = gathered === '0' ? false : true
    if(type === '0') {
        interesting.update({_id: id}, {$set:{
            gathered:newGathered
        }}, function(err, docs){
            if(err) {
                JSON.stringify({
                    code: 0,
                    msg: 'err'
                })
            } else {
                res.end(
                    JSON.stringify({
                        code: 1,
                        msg: 'success',
                        data: docs
                    })
                );
            }
        })
    }

    if(type === '1') {
        restaurant.update({_id: id}, {$set:{
            gathered:newGathered
        }}, function(err, docs){
            if(err) {
                JSON.stringify({
                    code: 0,
                    msg: 'err'
                })
            } else {
                res.end(
                    JSON.stringify({
                        code: 1,
                        msg: 'success',
                        data: docs
                    })
                );
            }
        })
    }

});


module.exports = router;
