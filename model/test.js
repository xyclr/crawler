/**
 * @Author goujianjun
 * @Since v1.0.0
 */

const mongoose = require('../utils/db')
const Schema = mongoose.Schema

/**
 * Restaurant info
 */
const testSchema = new Schema({
  url: {type: 'string'},
  name: {type: 'string'},
  gathered:  {type: 'boolean'},
})

module.exports = mongoose.model('test',testSchema);