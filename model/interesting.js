/**
 * @Author goujianjun
 * @Since v1.0.0
 */

const mongoose = require('../utils/db')
const Schema = mongoose.Schema

/**
 * Restaurant info
 */
const interesting = new Schema({
  url: {type: 'string'},
  mainPic: {type: 'object'}, // 主图
  title: {type: 'object'},
  map: {type: 'object'}, // 位置
  restaurantInfo: {type: 'object'}, // 餐台信息  name, tel, addr, classification
  content: {type: 'array'}, // 内容
  tags: {type: 'string'},
  regions: {type: 'array'},
  gathered:  {type: 'boolean'},
})

module.exports = mongoose.model('interesting',interesting);