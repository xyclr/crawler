/**
 * @Author goujianjun
 * @Since v1.0.0
 */

const mongoose = require('../utils/db')
const Schema = mongoose.Schema

/**
 * Restaurant info
 */
const restaurantSchema = new Schema({
  url: {type: 'string'},
  baseInfo:  {type: 'object'},
  gathered:  {type: 'boolean'},
  name: {type: 'string'}, // 点名
  nameCn: {type: 'string'}, // 点名
  start: {type: 'number'}, // 评星
  collectionCount: {type: 'number'}, // 收藏数量
  classification: {type: 'string'}, //分类
  mainPic: {type: 'string'}, // 主图
  position: {type: 'object'}, // 位置信息
  tel: {type: 'string'}, // 电话
  menu:{type: 'object'}, // 菜单
  menuRecommend:{type: 'array'}, // 菜单推荐
  special:{type: 'array'}, // 特别（非必选）
  brandStory:{type: 'array'}, // 特别（非必选）
  imgs:{type: 'array'}, // 餐厅图片
  extraInfo:{type: 'object'}, // 额外信息
  recommend:{type: 'array'}, // 会员推荐
  fit:{type: 'array'}, // 这家店适合
  comments:{type: 'array'}, // 评论
  regions:{type: 'array'}, // 评论
  commentsStart:{type: 'object'}, // 评论
})

module.exports = mongoose.model('restaurant',restaurantSchema);