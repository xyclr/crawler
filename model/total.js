/**
 * @Author goujianjun
 * @Since v1.0.0
 */

const mongoose = require('../utils/db')
const Schema = mongoose.Schema

/**
 * Restaurant info
 */
const totalSchema = new Schema({
  error: {type: 'array'},
  all: {type: 'array'},
  success: {type: 'array'},
})

module.exports = mongoose.model('total',totalSchema);